from setuptools import setup
from os import path

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(name='aioselenium',
      version='0.3.2',
      description='Very basic asyncio implementation of the Selenium RemoteWebdriver using JSON Wire protocol',
      url='https://gitlab.com/alexbode/aioselenium',
      author='Alex Bode',
      author_email='alexvincentbode@gmail.com',
      license='MIT',
      packages=['aioselenium'],
      long_description=long_description,
      long_description_content_type='text/markdown',
      install_requires=[
          'aiohttp',
      ],
      include_package_data=True,
      keywords='selenium webdriver asyncio aiohttp remote',
      zip_safe=False)
